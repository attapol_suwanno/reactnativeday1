/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import min from './min'

AppRegistry.registerComponent(appName, () => min);
